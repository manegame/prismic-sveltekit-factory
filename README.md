# Prismic Sveltekit Factory
*Generate Sveltekit routes and stores for prismic data*

### `TL;DR`
Simply run the command `factory` inside the folder where your masks are located as `json` files to get an output folder with contents

OR

Run the command `scaffold` in your base project to begin fresh.

## Features

* Generates deploy config assuming netlify adapter
* Generates routes based on your Prismic masks
  * API routes for data
  * Web routes for frontend
* Generates stores based on your Prismic masks
* Adds base components
  * Rich Text component for text
  * Image component for images
  * Slice Zone with
    * Paragraph
    * Image
    * Embed
    * Video File
* Prismic previews!

## Template

A basic mask of the following format is assumed for SEO.

```
{
  "Main": {
    "title": {
      "type": "StructuredText",
      "config": {
        "single": "heading1, heading2, heading3, heading4, heading5, heading6",
        "label": "Title"
      }
    },
    "image_seo": {
      "type": "Image",
      "config": {
        "constraint": {
          "width": 1240
        },
        "thumbnails": [],
        "label": "Image (SEO)"
      }
    },
    "description_seo": {
      "type": "StructuredText",
      "config": {
        "single": "paragraph,preformatted,heading1,heading2,heading3,heading4,heading5,heading6,strong,em,hyperlink,image,embed,list-item,o-list-item,rtl",
        "label": "Description (SEO)",
        "placeholder": "Max. 160 characters"
      }
    }
  }
}
```