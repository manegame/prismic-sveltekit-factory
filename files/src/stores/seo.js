import PrismicDom from 'prismic-dom'

// Defaults
export const DEFAULT_TITLE = ""
export const DEFAULT_DESCRIPTION = ""
export const DEFAULT_IMAGE = ""

/**
 * Utilities
 */
const hasData = (doc) => { return Object.keys(doc).includes('data') }
const truncate = (input, length) => input.length > length ? `${input.substring(0, length)}...` : input

/**
 * 
 * @param {*} doc 
 * @returns 
 */
const getTitle = (doc) => {
  return doc.data.title ? PrismicDom.RichText.asText(doc.data.title) : DEFAULT_TITLE
}

/**
 * 
 * @param {*} page 
 * @returns the right description for SEO
 */
const getDescription = (doc) => {
  let description

  if (hasData(doc) && doc.data.description) {
    description = PrismicDom.RichText.asText(doc.data.description)
  }

  if (hasData(doc) && doc.data.description_seo) {
    description = PrismicDom.RichText.asText(doc.data.description_seo)
  }

  if (!description) description = DEFAULT_DESCRIPTION

  description = truncate(description, 160)

  return description
}

/**
 * 
 * @param {*} page 
 * @returns the right image url for SEO
 */
const getImage = (doc) => {
  let image
  
  if (hasData(doc) && doc.data.cover_image && doc.data.cover_image.Large.url) {
    image = doc.data.cover_image.Large.url
  }
  
  if (hasData(doc) && doc.data.image_seo && doc.data.image_seo.url) {
    image = doc.data.image_seo.url
  }

  if (!image) image = DEFAULT_IMAGE

  return image
}

export {
  getTitle,
  getDescription,
  getImage
}