import Prismic from '@prismicio/client'
import linkResolver from "$utilities/linkResolver"

export async function get ({ query }) {
  let Location = '/'

  const previewToken = query.get('token')
  const documentId = query.get('documentId')

  const api = await Prismic.client(import.meta.env.VITE_PRISMIC_ROOT)
  Location = await api.getPreviewResolver(previewToken, documentId).resolve(linkResolver, '/')

  return {
    status: 302,
    headers: { Location }
  }
}