import Prismic from '@prismicio/client'

const Client = async (previewCookie) => {
  const c = await Prismic.client(import.meta.env.VITE_PRISMIC_ROOT, {
    req: {
      headers: {
        cookie: previewCookie,
      },
    }
  })

  return c
}

export default Client