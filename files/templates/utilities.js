import lodash from "lodash"
const { kebabCase } = lodash

import beautify from 'js-beautify'
const options = { indent_size: 2, brace_style: 'collapse-preserve-inline' }

const linkResolverTemplate = (typedContents) => {
  
  /**
   * Generate one link resolver clause
   * @param {*} content 
   * @returns 
   */
  const linkResolverClause = (content) => {
    if (content.repeatable) {
      return `
        case("${content.type}"):
          return "/${kebabCase(content.type)}s/" + doc.uid`
    } else {
      if (content.type === 'index' || content.type === 'home') {
        return `
          case("${content.type}"):
            return "/"`
      } else {
        return `
          case("${content.type}"):
            return "/${kebabCase(content.type)}"`
      }
    }
  }
  
  /**
   * The clauses as an array
   */
  const linkResolverClauses = typedContents.map(content => linkResolverClause(content))

  return beautify.js(`
    const linkResolver = function (doc) {
      // Pretty URLs for known types
      switch (doc.type) {
        ${linkResolverClauses.join("\r\n")}
        default:
          return "/"
      }
    };
    
    export default linkResolver  
  `, options)

}

export {
  linkResolverTemplate
}