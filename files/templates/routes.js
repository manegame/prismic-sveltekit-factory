import lodash from "lodash"
const { startCase, camelCase, kebabCase } = lodash

const singleType = type => { return camelCase(type) }
const singleRepeatableType = type => { return camelCase(type) }
const repeatableType = type => { return `all${pascalCase(type)}s` }

/**
 * Convert any string to PascalCase
 * @param {*} str 
 * @returns 
 */
const pascalCase = (str) => {
  return startCase(camelCase(str)).replace(/ /g, '')
}

/**
 * Template for a single route
 * @param {*} type 
 * @returns 
 */
const singleTemplate = (content) => {
  let uri = content.type === 'index' ? '/api.json' : `/api/${singleType(content.type)}.json`

  return `
    <script context="module">
      import { ${singleType(content.type)} } from "$stores/prismic"

      export async function load ({ fetch }) {
        const data = await fetch('${uri}')
        ${singleType(content.type)}.set(await data.json())

        return {}
      }
    </script>

    <script>${content.slices ? "import Slices from '$components/slices/Slices.svelte'" : ""};</script>

    <main>
      {JSON.stringify($${singleType(content.type)})}

      ${content.slices ? `<Slices slices={$${singleType(content.type)}.data.body} />` : ""}
    </main>

    <style lang="postcss">
    </style>
  `
}

/**
 * Template for a repeatable route
 * @param {*} type 
 * @returns 
 */
const repeatableTemplate = (content) => {
  return `
    <script context="module">
      import { ${repeatableType(content.type)} } from "$stores/prismic"

      export async function load ({ fetch, page }) {   
        const data = await fetch('/api/${kebabCase(content.type)}s.json')
        const result = await data.json()

        ${repeatableType(content.type)}.set(result)

        return {}
      }
    </script>

    <main>
      {JSON.stringify($${repeatableType(content.type)})}
    </main>

    <style lang="postcss">
    </style>
  `
}

/**
 * Template for a single repeatable route
 * @param {*} type 
 * @returns 
 */
const singleRepeatableTemplate = (content) => {
  return `
    <script context="module">
      import { ${singleRepeatableType(content.type)} } from "$stores/prismic"

      export async function load ({ fetch, page }) {
        const data = await fetch(\`/api/${kebabCase(content.type)}s/\${page.params.${content.type}}.json\`)
        const result = await data.json()

        ${singleRepeatableType(content.type)}.set(result)

        return {}
      }
    </script>

    <script>${content.slices ? "import Slices from '$components/slices/Slices.svelte'" : ""};</script>

    <main>
      {JSON.stringify($${singleRepeatableType(content.type)})}

      ${content.slices ? `<Slices slices={$${singleRepeatableType(content.type)}.data.body} />` : ""}
    </main>

    <style lang="postcss">
    </style>
  `
}

/**
 * Template for a layout
 * @returns 
 */
const layoutTemplate = () => {
  return `
<script context="module">
  import { index } from "$stores/prismic"

  export async function load ({ fetch }) {
    const data = await fetch('/api.json')
    const result = await data.json()

    // Set to store, never ever wait for data again :)
    index.set(result)

    return {}
  }
</script>

<script>
  import "../app.postcss"
  import MetaData from "$components/SEO/MetaData.svelte"
</script>

<MetaData />

<slot></slot>
  `
}

export {
  singleTemplate,
  repeatableTemplate,
  singleRepeatableTemplate,
  layoutTemplate
}