/**
 * App.html
 * @param {*} repo 
 * @returns 
 */
const appTemplate = (repo) => {
  return `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script async defer src="https://static.cdn.prismic.io/prismic.js?new=true&repo=${repo}"></script>
    %svelte.head%
  </head>
  <body>
    <div id="svelte">%svelte.body%</div>
  </body>
</html>  
`
}

const tomlTemplate = (repo) => {
  return `
[build]
  command = "yarn build"
  functions = "functions/"
  publish = "build/"

[functions]
  directory = "functions/"

[build.environment]
  VITE_PRISMIC_ROOT = "https://${repo}.cdn.prismic.io/api/v2"
  VITE_PRISMIC_URL = "https://${repo}.prismic.io"
`
}

export {
  appTemplate,
  tomlTemplate
}