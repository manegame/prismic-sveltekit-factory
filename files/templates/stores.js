import lodash from "lodash"
const { startCase, camelCase, kebabCase } = lodash

import beautify from 'js-beautify'
const options = { indent_size: 2, brace_style: 'collapse-preserve-inline' }

/**
 * Template output
 * @param {*} typedContents 
 * @returns 
 */
const storesTemplate = (typedContents) => {

  /**
   * Convert any string to PascalCase
   * @param {*} str 
   * @returns 
   */
  const pascalCase = (str) => {
    return startCase(camelCase(str)).replace(/ /g, '')
  }

  /**
   * Make repeatable things
   */
  typedContents.forEach(content => {
    let stores = []
    if (!content.repeatable) {
      stores.push(camelCase(content.type))
    } else {
      stores.push(camelCase(content.type))
      stores.push(`all${pascalCase(content.type)}s`)
    }

    content.stores = stores
  })

  /**
   * Create all store names as an array
   */
  const storeNames = typedContents.map((content) => {
    return content.stores
  }).flat()

  /**
   * Create a store definition string
   * @param {*} type 
   * @returns 
   */
  const storeDefinition = (type) => {
    return `
    // ${type}
    let ${type} = writable({})
    `
  }

  /**
   * Create a route condition for given route
   * 
   * @param {*} content 
   * @param {*} seoIdentifier 
   * @returns 
   */
  const routeCondition = (content, seoIdentifier) => {
    if (content.type === 'index' || content.type === 'home') {
      return `
      /** ${content.type} */
      if ($page.path === '/') {
        return get${seoIdentifier}($${camelCase(content.type)})
      }
      ` 
    }

    if (content.repeatable) {
      return `
      /** ${content.type} */
      if ($page.params.${content.type}) {
        return get${seoIdentifier}($${camelCase(content.type)})
      }

      if ($page.path === '${kebabCase(content.type)}s') {
        return get${seoIdentifier}($all${pascalCase(content.type)}s)
      }
      `
    }

    if (!content.repeatable) {
      return `
      /** ${content.type} */
      if ($page.path === '${kebabCase(content.type)}') {
        return get${seoIdentifier}($${camelCase(content.type)})
      }
      `
    }
  }

  /**
   * Make route conditions for a type
   * @param {*} seoIdentifier 
   * @returns 
   */
  const routeConditions = (seoIdentifier) => {
    return typedContents.map(content => {
      return routeCondition(content, seoIdentifier)
    })
  }

  /**
   * Return all store definition strings
   */
  const storeDefinitions = storeNames.map(storeName => storeDefinition(storeName))

  /**
   * Put it in action
   */
  return beautify.js(`
  /**
   * Imports
   */
  // STORES
  import { writable, derived } from "svelte/store"

  // SVELTEKIT STORES
  import { page } from "$app/stores"

  // SEO
  import {
    getTitle,
    DEFAULT_TITLE,
    getDescription,
    DEFAULT_DESCRIPTION,
    getImage,
    DEFAULT_IMAGE
  } from "./seo"
  
  /**
   * Initialise stores
   */
  ${storeDefinitions.join('\r\n')}
  
  /**
   * Make an array for the seo functions
   * TODO: Simplify structure
   */
  const allStores = [${storeNames.join(', ')}, page]
  
  /**
   * Title for SEO
   */
  let seoTitle = derived(allStores, ([$${storeNames.join(', $')}, $page]) => {
    ${routeConditions('Title').join('\r')}

    return DEFAULT_TITLE
  })
  
    
  /**
   * Description for SEO
   */
  let seoDescription = derived(allStores, ([$${storeNames.join(', $')}, $page]) => {
    ${routeConditions('Description').join('\r')}

    return DEFAULT_DESCRIPTION
  })
  

  /**
   * Image for SEO
   */
  let seoImage = derived(allStores, ([$${storeNames.join(', $')}, $page]) => {
    ${routeConditions('Image').join('\r')}

    return DEFAULT_IMAGE
  })


  
  
  export {
    ${storeNames.join(',\r\n')},
    // SEO
    seoTitle,
    seoDescription,
    seoImage
  }
  `, options)
}

export {
  storesTemplate
}