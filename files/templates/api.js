import lodash from "lodash"
const { startCase, camelCase, kebabCase } = lodash

import beautify from 'js-beautify'
const options = { indent_size: 2, brace_style: 'collapse-preserve-inline' }

const singleType = type => { return camelCase(type) }
const singleRepeatableType = type => { return camelCase(type) }
const repeatableType = type => { return `all${pascalCase(type)}s` }

const singleApiTemplate = (type) => {
  return beautify.js(`/**
 * Single
 */
import Client from "$utilities/client"

export async function get ({ headers }) {
  const api = await Client(headers.cookie)
  const response = await api.getSingle('${type}')

  if (response) {
    return {
      body: response
    }
  }

  return {
    error: new Error('could not fetch data')
  }
}
`, options)
}

const repeatableApiTemplate = (type) => {
  return beautify.js(`/**
 * Repeatable
 */
import Client from "$utilities/client"

export async function get ({ headers }) {
  const api = await Client(headers.cookie)
  const response = await api.query('[at(document.type, "${type}")]')

  if (response) {
    return {
      body: response
    }
  }

  return {
    error: new Error('could not fetch data')
  }
}
`, options)
}

const singleRepeatableApiTemplate = (type) => {
  return beautify.js(`/**
* Single Repeatable
*/
import Client from "$utilities/client"

export async function get ({ params, headers }) {
  const api = await Client(headers.cookie)
  const { ${type} } = params

  const response = await api.getByUID('${type}', ${type})
  
  if (response) {
    return {
      body: response
    }
  }

  return {
    error: new Error('could not fetch data')
  }
}
`, options)
}

export {
  singleApiTemplate,
  repeatableApiTemplate,
  singleRepeatableApiTemplate
}