#! /usr/bin/env node
import enquirer from 'enquirer'
const { prompt } = enquirer

/*
==========================
     'ELLO GUVNA
==========================
*/
const myArguments = process.argv.slice(2)
const scaffold = myArguments[0] === '--scaffold'
 
const response = await prompt({
  type: 'input',
  name: 'repo',
  message: 'Please enter the name of your Prismic repo'
})

const repo = response.repo


/*
==========================
        COALS
==========================
*/

import { dirname } from 'path'
import { fileURLToPath } from 'url'

import {
  execSync
} from "child_process"

import {
  readdir,
  readFileSync,
  writeFileSync,
  existsSync,
  mkdirSync,
  copyFileSync,
  rmSync
} from "fs"

import pkg from "ncp"
const { ncp } = pkg
ncp.limit = 16

import {
  layoutTemplate,
  singleTemplate,
  repeatableTemplate,
  singleRepeatableTemplate,
} from "./files/templates/routes.js"

import {
  singleApiTemplate,
  repeatableApiTemplate,
  singleRepeatableApiTemplate
} from "./files/templates/api.js"

import {
  storesTemplate
} from "./files/templates/stores.js"

import {
  linkResolverTemplate
} from "./files/templates/utilities.js"

import {
  appTemplate,
  tomlTemplate
} from "./files/templates/app.js"

import lodash from "lodash"

const { kebabCase } = lodash

/**
 * Paths, filenames
 */
const cwd = process.cwd() + "/"
const __dirname = dirname(fileURLToPath(import.meta.url)) + "/"

/**
==========================
      ARBEITERS 
==========================
 */

const dirOpen = (dirPath) => {
  let command = '';
  switch (process.platform) {
    case 'darwin':
      command = 'open';
      break;
    case 'win32':
      command = 'explore';
      break;
    default:
      command = 'xdg-open';
      break;
  }
  console.log('execSync', `${command} "${dirPath}"`);
  return execSync(`${command} "${dirPath}"`);
}

/**
 * File / folder path for masks
 * @param {*} uri 
 * @returns 
 */
const maskFilePath = (uri) => {
  return `${cwd}/masks/${uri}`
}


/**
 * File path for output
 * @param {*} uri 
 * @returns 
 */
const outputFilePath = (uri) => {
  return `${cwd}${scaffold ? '' : 'output/'}${uri}`
}


/**
 * Generate layout
 */
const generateLayout = () => {
  const filePath = outputFilePath('src/routes/__layout.svelte')
  const fileContents = layoutTemplate()

  writeFileSync(filePath, fileContents)
}

/**
 * 
 * @param {*} type 
 */
const generateSingleRoute = (content) => {
  const filePath = outputFilePath(`src/routes/${content.type}.svelte`)
  const fileContents = singleTemplate(content)

  writeFileSync(filePath, fileContents)
}

/**
 * 
 * @param {*} type 
 */
const generateSingleApiRoute = (type) => {
  const filePath = outputFilePath(`src/routes/api/${type}.json.js`)
  const fileContents = singleApiTemplate(type)

  writeFileSync(filePath, fileContents)
}

/**
 * 
 * @param {*} type 
 */
const generateRepeatableRoute = (content) => {
  const filePath = outputFilePath(`src/routes/${kebabCase(content.type)}s/index.svelte`)
  const fileContents = repeatableTemplate(content)

  writeFileSync(filePath, fileContents)
}

/**
 * 
 * @param {*} type 
 */
const generateRepeatableApiRoute = (type) => {
  const filePath = outputFilePath(`src/routes/api/${kebabCase(type)}s/index.json.js`)
  const fileContents = repeatableApiTemplate(type)

  writeFileSync(filePath, fileContents)
}

/**
 * 
 * @param {*} type 
 */
const generateSingleRepeatableRoute = (content) => {
  // For the single ones
  const filePath = outputFilePath(`src/routes/${kebabCase(content.type)}s/[${content.type}].svelte`)
  const fileContents = singleRepeatableTemplate(content)

  writeFileSync(filePath, fileContents)
}

/**
 * 
 * @param {*} type 
 */
const generateSingleRepeatableApiRoute = (type) => {
  // For the single ones
  const filePath = outputFilePath(`src/routes/api/${kebabCase(type)}s/[${type}].json.js`)
  const fileContents = singleRepeatableApiTemplate(type)

  writeFileSync(filePath, fileContents)
}

/**
 * Generate the store file
 * @param {*} typedContents 
 */
const generateStore = (typedContents) => {
  const filePath = outputFilePath(`src/stores/prismic.js`)
  const fileContents = storesTemplate(typedContents)

  writeFileSync(filePath, fileContents)
}

/**
 * Generate the store file
 * @param {*} typedContents 
 */
const generateLinkResolver = (typedContents) => {
  const filePath = outputFilePath(`utilities/linkResolver.js`)
  const fileContents = linkResolverTemplate(typedContents)

  writeFileSync(filePath, fileContents)
}

/**
 * Generate the store file
 * @param {*} typedContents 
 */
const generateAppHtml = (repo) => {
  const filePath = outputFilePath(`app.html`)
  const fileContents = appTemplate(repo)

  writeFileSync(filePath, fileContents)
}

/**
 * Generate the store file
 * @param {*} typedContents 
 */
const generateNetlifyToml = (repo) => {
  const filePath = outputFilePath(`netlify.toml`)
  const fileContents = tomlTemplate(repo)

  writeFileSync(filePath, fileContents)
}


/**
 * Make the initial folder structure
 */
const makeFolderStructure = () => {

  if (scaffold && (existsSync(outputFilePath('src')) || existsSync(outputFilePath('utilities')))) {
    console.log('\x1b[36m%s\x1b[0m', `

  !!!   Factory accident   !!!
  ============================
  `)
    console.log(`
    You should not have the src or utilities folders before running this command!

    Remove them and try again.

  `)
    process.exit()
  }

  // If the target is not scaffolding the current project,
  // this command will create the output folder for output
  if (!scaffold) {
    mkdirSync(cwd + '/output')
  }

  mkdirSync(outputFilePath('src'))
  mkdirSync(outputFilePath('src/routes'))
  mkdirSync(outputFilePath('src/routes/api'))
  mkdirSync(outputFilePath('src/stores'))
  mkdirSync(outputFilePath('src/components'))
  mkdirSync(outputFilePath('src/hooks'))
  mkdirSync(outputFilePath('utilities'))
}

/**
 * 
 * @param {*} type 
 */
const makeRepeatableFolderStructure = (type) => {
  mkdirSync(outputFilePath(`src/routes/${kebabCase(type)}s`))
}

/**
 * 
 * @param {*} type 
 */
const makeRepeatableApiFolderStructure = (type) => {
  mkdirSync(outputFilePath(`src/routes/api/${kebabCase(type)}s`))
}


/**
 * 
 * @param {*} file 
 * @param {*} data 
 */
 const generateRoute = (content) => {
  if (!content.repeatable) {
    generateSingleRoute(content)
  } else {
    makeRepeatableFolderStructure(content.type)
    generateRepeatableRoute(content)
    generateSingleRepeatableRoute(content)
  }
}

/**
 * 
 * @param {*} file 
 * @param {*} data 
 */
 const generateApiRoute = (content) => {
  if (!content.repeatable) {
    generateSingleApiRoute(content.type)
  } else {
    makeRepeatableApiFolderStructure(content.type)
    generateRepeatableApiRoute(content.type)
    generateSingleRepeatableApiRoute(content.type)
  }
}

/**
 * 
 * @param {*} types 
 */
const generateTypedContents = (files) => {
  let typedContents = []

  files.forEach((fileName) => {
    // Skip hidden files
    if (fileName.charAt(0) === '.') return
    // Skip everything that is non JSON
    const extensionPattern = /(?:\.([^.]+))?$/
    if (extensionPattern.exec(fileName)[1] !== 'json') return

    process.stdout.write("processing: ")
    console.log('\x1b[36m%s\x1b[0m', fileName)
    const type = fileName.replace('.json', '')
    
    const fileContents = JSON.parse(readFileSync(maskFilePath(fileName))).Main

    typedContents.push({
      ...fileContents,
      type,
      repeatable: !!fileContents.uid,
      slices: !!fileContents.body
    })
  })

  return typedContents
}


/**
 * Initialize
 */
const init = () => {
  // Remove existing output, only before scaffolding
  if (existsSync(cwd + 'output')) {
    rmSync(cwd + 'output', { recursive: true })
  }

  // Initialize
  makeFolderStructure()

  // Get going!
  try {
    readdir(maskFilePath(''), (err, files) => {
      if (err) throw err;

      // Typed contents
      const typedContents = generateTypedContents(files)

      // Src
      ncp(__dirname + `files/src`, outputFilePath(`src`), function (err) {
        if (err) {
          return console.error(err);
        }
      })

      // Utilities
      ncp(__dirname + `files/utilities`, outputFilePath(`utilities`), function (err) {
        if (err) {
          return console.error(err);
        }
      })

      // Layout
      generateLayout()

      // Routes
      typedContents.forEach(content => {
        generateRoute(content)
        generateApiRoute(content)
      })

      // Store
      generateStore(typedContents)

      // Link resolver
      generateLinkResolver(typedContents)

      // App.html
      generateAppHtml(repo)

      // App.html
      generateNetlifyToml(repo)

      /*
       *** 
      */

      console.log(`

  Production completed!
  =====================
`)

      console.log('\x1b[36m%s\x1b[0m', `
      __  . .* ,
      ~#@#%(" .,$ @
      ."^ ';"
     ..
    ;. :                                   . .
    ;==:                     ,,   ,.@#(&*.;'
    ;. :                   .;#$% & ^^&
    ;==:                   &  ......
    ;. :                   ,,;      :
    ;==:  ._______.       ;  ;      :
    ;. :  ;    ###:__.    ;  ;      :
---------------------.'  \`._;       :  :__.' .'        \`.-----------
-----------------------------------------------------------------------

Art by Alexander Antsiferov 

      `)

      dirOpen(scaffold ? cwd : cwd + 'output')
    })
  
  } catch (error) {
    console.error(error)
  }
}


console.log('\x1b[36m%s\x1b[0m', `

  *** Approaching factory ***
  ===========================

  https://gitlab.com/manegame

`)

console.log('\x1b[36m%s\x1b[0m', `

  Pushing coals into the oven!
  ============================

`)

console.log('\x1b[36m%s\x1b[0m', `
  ) ) )                     ) ) )
  ( ( (                      ( ( (
  ) ) )                       ) ) )
  (~~~~~~~~~)                 (~~~~~~~~~)
  | POWER |                   | POWER |
  |       |                   |       |
  I      _._                  I       _._
  I    /'   \`\\                I     /'   \`\\
  I   |   N    |               I    |   N   |
  f   |   |~~~~~~~~~~~~~~|    f     |    |~~~~~~~~~~~~~~|
  .'    |   ||~~~~~~~~|    |  .'     |    | |~~~~~~~~|   |
  /'------|---||--###---|----|/'-------|----|-|--###---|---|
-----------------------------------------------------------------------

Art by Unknown Artist
`)

init()
